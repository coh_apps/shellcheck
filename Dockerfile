ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

RUN apk update && \
    apk add shellcheck

ENTRYPOINT [ "/usr/bin/shellcheck" ]